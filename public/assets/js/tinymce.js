 

$(document).ready(function() {
    initTinyMce()
    //tinymce.get('mytextarea').setContent('<p>This is my new content!</p>');
    //write not working here
});

function initTinyMce(){
    tinymce.init({
        selector: '#mytextarea',
        height: 400,
        plugins: 'table image print',
        //menubar: 'insert',
        //toolbar: 'image',
        image_uploadtab: true,
        images_upload_url: {"location": linked },
        extended_valid_elements: 'a[href|target=_blank]',
        //selector: 'textarea#custom-toolbar-menu-button',
        //height: 500,
        toolbar: 'mybutton',
      
        setup: function (editor) {
          /* Menu items are recreated when the menu is closed and opened, so we need
             a variable to store the toggle menu item state. */
          var toggleState = false;
      
          /* example, adding a toolbar menu button */
          editor.ui.registry.addMenuButton('mybutton', {
            text: 'Tanda Tangan',
            fetch: function (callback) {
              var items = [
                {
                  type: 'menuitem',
                  text: 'Ttd Direktur',
                  onAction: function () {
                    editor.insertContent('&nbsp;<span class="ttd_dir ttd">[[ttd Direktur]]</span>');
                  }
                },
                {
                type: 'menuitem',
                text: 'Ttd Manager',
                onAction: function () {
                    editor.insertContent('&nbsp;<span class="ttd_man ttd">[[ttd Manager]]</span>');
                }
                },
                {
                type: 'menuitem',
                text: 'Ttd Behendara',
                onAction: function () {
                    editor.insertContent('&nbsp;<span class="ttd_ben ttd">[[ttd Behendara]]</span>');
                }
                },
                {
                type: 'menuitem',
                text: 'Ttd Sekertaris',
                onAction: function () {
                    editor.insertContent('&nbsp;<span class="ttd_sek ttd">[[ttd Sekertaris]]</span>');
                }
                },
                // {
                //   type: 'nestedmenuitem',
                //   text: 'Menu item 2',
                //   icon: 'user',
                //   getSubmenuItems: function () {
                //     return [
                //       {
                //         type: 'menuitem',
                //         text: 'Sub menu item 1',
                //         icon: 'unlock',
                //         onAction: function () {
                //           editor.insertContent('&nbsp;<em>You clicked Sub menu item 1!</em>');
                //         }
                //       },
                //       {
                //         type: 'menuitem',
                //         text: 'Sub menu item 2',
                //         icon: 'lock',
                //         onAction: function () {
                //           editor.insertContent('&nbsp;<em>You clicked Sub menu item 2!</em>');
                //         }
                //       }
                //     ];
                //   }
                // },
                {
                  type: 'togglemenuitem',
                  text: 'Toggle menu item',
                  onAction: function () {
                    toggleState = !toggleState;
                    editor.insertContent('&nbsp;<em>You toggled a menuitem ' + (toggleState ? 'on' : 'off') + '</em>');
                  },
                  onSetup: function (api) {
                    api.setActive(toggleState);
                    return function() {};
                  }
                }
              ];
              callback(items);
            }
          });
      
        },
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
    });
}

// tinymce.init({
//     selector: '#description',
//     extended_valid_elements : 'span',
//     verify_html: true,
//     height: 400,
//     theme: 'modern',
//     plugins: [
//         'advlist autolink lists link image charmap print preview hr anchor pagebreak',
//         'searchreplace wordcount visualblocks visualchars code fullscreen',
//         'insertdatetime media nonbreaking save table contextmenu directionality',
//         'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
//     ],
//     toolbar1: 'undo redo | insert | styleselect | fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
//     toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | mybutton',
//     image_advtab: true,
//     image_title: true,
//     image_caption: true,
//     relative_urls: false,
//     theme_advanced_buttons1: "forecolor,backcolor,fontselect,fontsizeselect",

//     templates: [
//         { title: 'Test template 1', content: 'Test 1' },
//         { title: 'Test template 2', content: 'Test 2' },
//     ],
// });

function cetak(){
    //read into
    tinyMCE.triggerSave();
    $('#summernote-hasil').html($("#mytextarea").val())
    $('.ttd_dir').html(`<img src="`+ttd1+`" style="width:100px"/>`)
    $('.ttd_man').html(`<img src="`+ttd2+`" style="width:100px"/>`)
    $('.ttd_ben').html(`<img src="`+ttd3+`" style="width:100px"/>`)
    $('.ttd_sek').html(`<img src="`+ttd4+`" style="width:100px"/>`)
    console.log($("#mytextarea").val())
    //write into
    $(tinymce.get('mytextarea').getBody()).html('<p>This is my new content!</p>'); //this working on click
}

function kopsurat(){
    var kop=`
    <div class="col-12" style="display: flex;border-bottom:2px solid #000">
        <div class="col-2" style="margin-top:3rem;padding-right:2rem">
            <img src="`+logo+`" style="width:100px"/>
        </div>
        <div class="col-10">
            <h1 style="margin-top:1rem">Metronic Academy</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, cum! Porro, error nisi velit eos reiciendis, adipisci illo laboriosam repellendus ut cupiditate qui et in voluptates inventore asperiores mollitia, quae tenetur fuga quidem neque blanditiis quos ipsa quia rem! Officiis est quisquam magni explicabo ratione aliquid, recusandae nulla atque autem!</p>
        </div>
    </div>
    type here
    `
    $(tinymce.get('mytextarea').getBody()).html(kop);
}