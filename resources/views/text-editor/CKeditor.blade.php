<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Perview Laporan | Dishub Penomoran</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

		<!-- <link href="{{ url('assets/plugin/summernote-0.8.18') }}/summernote.css" rel="stylesheet" type="text/css" /> -->
		<script src="https://cdn.ckeditor.com/ckeditor5/27.0.0/decoupled-document/ckeditor.js"></script>
        <script>
            var baseUrl = "{{url('/')}}/";
            
        </script>
        <style>
            .ck-editor__editable_inline {
                min-height: 500px;
            }
        </style>
	</head>
	<body>
    <div>Hello CKeditor</div>
    <h1>Document editor</h1>

    <!-- The toolbar will be rendered in this container. -->
    <div id="toolbar-container"></div>

    <!-- This container will become the editable. -->
    <div id="editor">
        <p>This is the initial editor content.</p>
    </div>

    <button onclick="cetak()" class="btn btn-success">Cetak</button>
	</body>
    <script src="{{ url('assets/js') }}/ckeditor.js"></script>
</html>