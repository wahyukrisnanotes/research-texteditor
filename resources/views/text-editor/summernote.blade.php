<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Perview Laporan | Dishub Penomoran</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

		<link href="{{ url('assets/plugin/summernote-0.8.18') }}/summernote.css" rel="stylesheet" type="text/css" />
		<script src="{{ url('assets/plugin/summernote-0.8.18') }}/summernote.js"></script>
        <script>
            var baseUrl = "{{url('/')}}/";
            
        </script>
        <style>
            img{
                width:100px;
            }
            table{
                width:80% !important;
            }
        </style>
	</head>
	<body>
    <div id="summernote">Hello Summernote</div>
    <div id="summernote-hasil"></div>
    <button onclick="cetak()" class="btn btn-success">Cetak</button>
	</body>
    <script src="{{ url('assets/js') }}/summernote.js"></script>
</html>