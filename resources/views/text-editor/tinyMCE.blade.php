<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Perview Laporan | Dishub Penomoran</title>
		<meta name="description" content="Login page example" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        {{--<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>--}}

		<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
            var baseUrl = "{{url('/')}}/";
            var linked="{{ url('assets/js') }}"
        </script>
        <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #summernote-hasil, #summernote-hasil * {
                visibility: visible;
            }
            #summernote-hasil {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
        </style>
	</head>
	<body>
    <div class="mb-10">Hello Tiny MCE</div>

    <h5 style="margin-top:2rem;font-weight:bolder">Contoh Kopsurat</h5>
    <div class="col-12" style="display: flex;border-bottom:2px solid #000">
        
        <div class="col-2" style="margin-top:3rem;padding-right:2rem;">
            <img src="{{url('assets/img/logo.png')}}" style="width:100px"/>
        </div>
        <div class="col-10">
            <h1 style="margin-top:1rem">Metronic Academy</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea, Porro, error nisi velit eos reiciendis, adipisci illo laboriosam repellendus ut cupiditate qui et in voluptates inventore asperiores mollitia, quae tenetur fuga quidem neque blanditiis quos ipsa quia rem! Officiis est quisquam magni explicabo ratione aliquid, recusandae nulla atque autem!</p>
        </div>
    </div>

    <textarea id="mytextarea" style="height:100vh"></textarea>
    <div style="margin-top:3rem">
        <button onclick="kopsurat()" class="btn btn-primary mr-5">Cetak Kopsurat</button>
        <button onclick="cetak()" class="btn btn-success mr-5">Cetak sebagai HTML</button>
        <button onclick="window.print()" class="btn btn-danger">Cetak</button>
    </div>
    <h5 style="margin-top:3rem">Hasil Cetak</h5>
    <div id="summernote-hasil" class="p-4"></div>
	</body>
    <script>
        var logo="{{url('assets/img/logo.png')}}"
        var ttd1="{{url('assets/img/ttd1.jpg')}}"
        var ttd2="{{url('assets/img/ttd2.jpg')}}"
        var ttd3="{{url('assets/img/ttd3.jpg')}}"
        var ttd4="{{url('assets/img/ttd4.webp')}}"
    </script>
    <script src="{{ url('assets/js') }}/tinymce.js"></script>
</html>